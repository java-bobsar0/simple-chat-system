# Simple Chatting System

A Client/Server chatting system implemented in Java using multi-threaded architecture

## Specifications

### Server
The first component is a multi-threaded server. This spawns threads to handle incoming requests in parallel.
Locks are used to make the chat server thread-safe.
#### Server Specifications
1. The server can handle connections by multiple clients.
2. The server accepts clients connecting through a TCP/IP connection, using Java’s Socket API.
3. The server can receive messages from clients.
4. The server broadcasts all messages received to all connected clients.
5. The server continues running if one or more clients disconnect from it.
6. The server shuts down cleanly if its user enters EXIT on the terminal.
7. The server uses 14001 as a default port.
8. The server’s implementation contains a class called ChatServer, allowing the software to start
   by running the following on the command-line:
   `java ChatServer`
9. You can pass an optional parameter, called csp, to ChatServer to request that it bind to another
   port. For example:
   `java Chat Server −csp 14005`
   
### Client
   The second component is a client. The client is responsible for sending and receiving messages from the server. Think of it as any Instant Messaging (IM) application, for example
   WhatsApp or MSN Messenger.
#### Client Specifications
1. The client can read messages from the console.
2. The client can send messages to a server.
3. The client can receive messages sent from the server.
4. The client can output all messages received by the server.
   - The client supports reading input from the console and displaying messages at the same time,
   by using a multi-threaded solution
5. The client connects to the server through a TCP/IP connection, established using Java’s
   Socket API.
6. The client sends and receives any messages through the socket connection established in the
   step above.
7. The client’s implementation contains a class called ChatClient, allowing the execution of the
   software by running the following on the command-line:
   `java ChatClient`
8. The client should use localhost as a default IP address to try to connect to.
9. The client should use 14001 as a default port.
10. You can pass an optional parameter, called cca, to ChatClient to request that it bind to another
    IP address. For example:
    `java Chat Client −cca 192.168.10.250`
11. You can pass an optional parameter, called ccp, to ChatClient to request that it bind to another
    port. For example:
    java Chat Client −ccp 14005
12. You can pass both the IP address and port of the server. For example:
    `java Chat Client −cca 192.168.10.250 −ccp 14005`

### Advanced Features

#### Simple Chat Bot

User(s) can talk to a chat bot that provides simple (e.g. pre-scripted) responses to user interactions. See for example https://rubberduckdebugging.com/. 
The chat bot must be run as a client (i.e. run with an IP address and port provided as command line arguments, then connect to the specified server).
Conversations with the chat bot should begin with `@bot`.

#### Private Messaging

User(s) can message other users privately by starting conversation with the `@` symbol followed by the user name. For example:

```
> [Steve <GROUP>] Hello
> [Great <GROUP>] Hello all.
> [Light <GROUP>] Hello!
> [Chioma <PRIVATE>] @Steve Go for a walk later?
> [Steve <PRIVATE>] @Chioma Sure...
```