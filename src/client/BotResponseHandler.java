package client;

import java.util.ArrayList;
import java.util.List;

public class BotResponseHandler {
    /**
     * Returns a list containing questions/answer pairs
     * When a question corresponding to the user input is determined, the answer to that question is returned
     *
     * @return [List<client.AIResponsePair>]
     */
    public static List<AIResponsePair> getResponses() {
        List<AIResponsePair> aiResponses = new ArrayList<>();

        aiResponses.add(
                new AIResponsePair(List.of("and you", "how are you", "hope you are good", "are you ok", "is it going"), "I'm good thanks. Hope you are too.")
        );
        aiResponses.add(
                new AIResponsePair(List.of("private chat", "private", "chat to individual"), "For a private session, begin your chat with '@' followed by name of the user. For eg: @Steve hello Steve")
        );
        aiResponses.add(
                new AIResponsePair(List.of("group chat", "message group", "group"), "Just type a message and it will be broadcast to the group by default. Hope that helps")
        );
        aiResponses.add(
                new AIResponsePair(List.of("no", "nonsense", "not really", "not sure"), "Hmm.. Interesting. I'm listening")
        );
        aiResponses.add(
                new AIResponsePair(List.of("yes"), "Good to hear!")
        );
        aiResponses.add(
                new AIResponsePair(List.of("thank"), "No worries at all.")
        );
        aiResponses.add(
                new AIResponsePair(List.of("bye", "nice chatting"), "Thanks for chatting")
        );
        aiResponses.add(
                new AIResponsePair(List.of("hello", "holla", "hi", "hiya"), "Hi, how are you?")
        );
        aiResponses.add(
                new AIResponsePair(List.of("m good", "m very good", "m well", "m great", "m awesome"), "That's nice.")
        );
        aiResponses.add(
                new AIResponsePair(List.of("not good", "not so good", "not well", "not feeling well", "not great", "not ok"), "Sorry to hear that. Go on and tell me all about it")
        );
        return aiResponses;
    }
}

/**
 * Holds a pair of predetermined list of questions and corresponding answer to the questions
 */
class AIResponsePair {
    private final List<String> questions;

    private final String answer;

    public AIResponsePair(List<String> questions, String answer) {
        this.questions = questions;
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public List<String> getQuestions() {
        return questions;
    }
}