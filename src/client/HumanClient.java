package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class that represents a human client.
 * This is instantiated in the HumanClientMain class when the program is started
 */
public class HumanClient extends ChatClientAbstract {

    private final BufferedReader stdIn;

    public HumanClient(String[] args) throws IOException {
        super(args);
        // alert the server that the client is a human
        out.println(ClientType.HUMAN);
        stdIn = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * A thread that reads input from user via the command-line and sends to server
     *
     * @return separate [Thread]
     */
    Thread processUserInputThread() {
        return new Thread(() -> {
            try {
                while (true) {
                    String fromUser = stdIn.readLine();
                    if (fromUser == null || fromUser.equalsIgnoreCase(EXIT_COMMAND)) {
                        // send the chat message to the server through the output stream attached to the socket
                        out.println(EXIT_COMMAND);
                        break;
                    }
                    out.println(fromUser.trim());
                }
            } catch (IOException e) {
                System.err.println("Error occurred while trying to read user input. Check that server is up and running. Exiting program...");
                System.exit(1);
            }
        });
    }

    /**
     * Reads input from server and prints to console
     *
     * @throws IOException if there's an error with reading input from the socket
     */
    @Override
    public void processServerInput() throws IOException {
        String fromServer;
        // listening for server input
        while ((fromServer = in.readLine()) != null) {
            ClientMessage messageObj = ClientMessage.fromString(fromServer);

            System.out.println(messageObj.getFormattedMessage());
        }
        System.exit(0);
    }

    /**
     * Closes this resource, relinquishing any underlying resources.
     * This method is invoked automatically on objects managed by the
     * {@code try}-with-resources statement
     *
     * @throws IOException if this resource cannot be closed
     */
    @Override
    public void close() throws IOException {
        super.close();
        stdIn.close();
    }
}