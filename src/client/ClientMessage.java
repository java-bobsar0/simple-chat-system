package client;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.FormatStyle;
import java.util.Arrays;
import java.util.Base64;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Contains message to be displayed on client terminal
 */
public class ClientMessage {
    private final String message;
    private final String sender;
    private final String time;
    private final String chatType;


    public ClientMessage(String message, String sender, String time, String chatType) {
        this.message = message;
        this.sender = sender;
        this.time = time;
        this.chatType = chatType;
    }

    public static ClientMessage fromString(String string) {
        // convert string representation of message (sent by server) to map
        try {
            Map<String, String> map = Arrays.stream(string.split(","))
                    .map(s -> s.split("=", 2))
                    .collect(Collectors.toMap(s -> s[0], s -> s[1]));

            // decode message
            String decodedMsg = decodeString(map.get("message"));
            String decodedSender = decodeString(map.get("sender"));
            String decodedTime = decodeString(map.get("dateTime"));
            String decodedChatType = decodeString(map.get("chatType"));

            return new ClientMessage(decodedMsg, decodedSender, decodedTime, decodedChatType);

        } catch (Exception e) {
            return new ClientMessage(string, null, null, null);
        }
    }

    private static String decodeString(String string) {
        if (string == null) return null;
        //check if string is already encoded: https://stackoverflow.com/questions/8571501/how-to-check-whether-a-string-is-base64-encoded-or-not
        String regex = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$";
        if (string.matches(regex)) {
            byte[] bytesArr = Base64.getDecoder().decode(string);
            return new String(bytesArr);
        }
        return string;
    }

    public String getSender() {
        return sender;
    }

    public String getFormattedMessage() {
        if (sender.equalsIgnoreCase("server")) {
            return String.format("[%s] %s", sender, message);
        }
        return String.format("%s: [%s <%s>] %s", getFormattedTime(), sender, chatType, message);
    }

    public String getPlainMessage() {
        return message;
    }

    private String getFormattedTime() {
        try {
            // format time
            LocalDateTime dateTime = LocalDateTime.parse(time);
            DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
            return dateTime.format(formatter);
        } catch (DateTimeParseException e) {
            return null;
        }
    }
}