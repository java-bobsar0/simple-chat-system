package client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents a bot client.
 * This is instantiated in the client.BotClientMain class when the program is started
 */
public class BotClient extends ChatClientAbstract {

    List<BotClientProcessInputThread> botThreads = new ArrayList<>();

    public BotClient(String[] args) throws IOException {
        super(args);
        // alert the server that the client is a bot
        out.println(ClientType.BOT);
        System.out.println("Bot is up and running...");
    }

    @Override
    public void sendOutputToServer(String recipient, String message) {
        out.println(String.format("@%s %s", recipient, message));
    }

    /**
     * Process input message from server
     *
     * @throws IOException if I/O error in reading input or sending output messages
     */
    @Override
    public void processServerInput() throws IOException {
        String fromServer;
        // listening for input message from server
        while ((fromServer = in.readLine()) != null) {
            ClientMessage messageObj = ClientMessage.fromString(fromServer);
            System.out.println(messageObj.getFormattedMessage());

            String sender = messageObj.getSender();
            String response = getBotResponse(messageObj.getPlainMessage(), sender);
            //print response and send response to sender of the input message
            sendOutputToServer(sender, response);
            System.out.printf("[BOT -> %s] %s%n", sender, response);
        }
        System.exit(0);
    }

    /*
     Handle each distinct conversation with a sender using a thread
     */
    private String getBotResponse(String input, String sender) {
        // check if list of threads contain the sender. If it does, use existing thread to continue communication with sender
        BotClientProcessInputThread botThread = botThreads.stream()
                .filter(thread -> thread.getSender().equalsIgnoreCase(sender))
                .findFirst()
                .orElse(null);

        if (botThread == null) {
            // create new thread to begin new communication as no existing communication with sender
            botThread = new BotClientProcessInputThread(sender, input.trim());
            botThreads.add(botThread);
            botThread.start();
        } else {
            botThread.setInputMessage(input);
        }
        return botThread.getOutputMessage();
    }
}