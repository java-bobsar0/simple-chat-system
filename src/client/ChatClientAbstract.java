package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Abstract class via which client.HumanClient and client.BotClient implementations are derived.
 * Implements AutoCloseable interface so that resources can be automatically closed in a try-with-resources statement at the main functions where the implementations are instantiated
 */
public abstract class ChatClientAbstract implements AutoCloseable {

    protected static String EXIT_COMMAND = "bye";
    protected final PrintWriter out;
    protected final BufferedReader in;
    private final Socket chatSocket;

    public ChatClientAbstract(String[] args) throws IOException {
        // open a socket connected to the server on the specified hostName and portNumber
        chatSocket = new Socket(getHostName(args), getPortNumber(args));
        out = new PrintWriter(chatSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(chatSocket.getInputStream()));
    }

    public String getHostName(String[] valuesArr) throws IllegalArgumentException {
        String hostName;
        if (valuesArr.length > 0) {
            for (int i = 0; i < valuesArr.length; i++) {
                if (valuesArr[i].equals("-cca")) {
                    if (i < valuesArr.length - 1 && !valuesArr[i].equals("-ccp")) {
                        hostName = valuesArr[i + 1];
                        return hostName;
                    }
                    throw new IllegalArgumentException("Host name not found. Please include hostname after '-cca' tag or leave out tag to use default hostname");
                }
            }
        }
        return "localhost";
    }

    public int getPortNumber(String[] valuesArr) throws IllegalArgumentException {

        if (valuesArr.length > 0) {
            for (int i = 0; i < valuesArr.length; i++) {
                if (valuesArr[i].equals("-ccp")) {
                    if (i < valuesArr.length - 1) {
                        try {
                            int portNumber = Integer.parseInt(valuesArr[i + 1]);
                            if (portNumber < 1024 || portNumber > 65535) {
                                System.err.println("Please choose a port number between 1024 and 65,535");
                                System.exit(1);
                            }
                            return portNumber;
                        } catch (NumberFormatException e) {
                            System.err.println("Error in parsing port number. Ensure that the port number is a valid integer");
                            System.exit(1);
                        }
                    }
                    throw new IllegalArgumentException("Port number not found. Please include port number after '-ccp' tag or leave out tag to use default port number");
                }
            }
        }
        return 14001;
    }

    public void sendOutputToServer(String recipient, String message) {
        out.println(message);
    }

    abstract void processServerInput() throws IOException;

    @Override
    public void close() throws IOException {
        chatSocket.close();
        out.close();
        in.close();
    }

    protected enum ClientType {
        HUMAN,
        BOT
    }
}