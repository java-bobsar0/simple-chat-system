package client;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Run the client-side program from this class to startup a Human Client
 */
public class HumanClientMain {

    public static void main(String[] args) {
        // Use try-with-resources syntax to ensure that resources are automatically closed after program exits
        try (HumanClient humanClient = new HumanClient(args)) {

            humanClient.processUserInputThread().start();

            humanClient.processServerInput();

        } catch (UnknownHostException e) {
            System.err.println("Unknown Host Error: " + e.getMessage());
            System.exit(1);
        } catch (IllegalArgumentException e) {
            System.err.println("Invalid port number. Ensure port number is between 0 and 65535, inclusive.");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("I/O connection error: " + e.getMessage());
            System.err.println("Please check that server is up and running and what port it's on. If server is up and problem persists, restart your program.");
            System.exit(1);
        }
    }
}