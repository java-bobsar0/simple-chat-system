package client;

import java.util.List;

/**
 * Thread class that processes messages received by the bot client
 * Each thread class handles conversations with a particular human client
 */
public class BotClientProcessInputThread extends Thread {

    private static final int WAITING = 0;
    private static final int SENT_WELCOME_MSG = 1;
    private final String sender;
    private int state;
    private String inputMessage;
    private String outputMessage;


    public BotClientProcessInputThread(String sender, String message) {
        this.sender = sender;
        inputMessage = message;
        state = WAITING;
        outputMessage = "Hi " + sender + ", nice to e-meet you. I'm the chat BOT. My scope is a bit limited so I can only answer basic queries like how to send a private or group chat. Can also tell you how I'm feeling at the moment :). Remember to begin your chat with '@bot' to talk to me specifically!";
    }

    @Override
    public void run() {
        processInput();
    }

    // this is called whenever the input message is set/changed. response will only be derived from a pair of aiResponses when welcome message is sent
    private void processInput() {

        if (state == WAITING) {
            state = SENT_WELCOME_MSG;
        } else if (state == SENT_WELCOME_MSG) {

            List<AIResponsePair> aiResponses = BotResponseHandler.getResponses();
            // check if any of the questions in the responses are contained within the input, if a matching question is found, answer is returned
            for (AIResponsePair aiResponsePair : aiResponses) {
                long questionCount = aiResponsePair.getQuestions().stream()
                        .filter(question -> inputMessage.toLowerCase().contains(question))
                        .count();
                if (questionCount > 0) {
                    outputMessage = aiResponsePair.getAnswer();
                    return;
                }
            }
            outputMessage = "I'm sorry, I don't seem to understand you. I'm still listening...";
        }
    }

    public void setInputMessage(String inputMessage) {
        this.inputMessage = inputMessage;
        processInput();
    }

    public String getOutputMessage() {
        return outputMessage;
    }

    public String getSender() {
        return sender;
    }
}