package server;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Base64;

/**
 * Server-side Message class
 */
public class Message {
    private final LocalDateTime dateTime = LocalDateTime.now();
    private final String message;
    protected String sender;
    private ChatType chatType;

    public Message(String message, ChatType chatType, String sender) {
        this.chatType = chatType;
        this.message = message;
        this.sender = sender;
    }

    public Message(String message, String sender) {
        this.message = message;
        this.sender = sender;
    }

    /**
     * Constructs a message object communicated directly by the server
     *
     * @param message message to send
     * @return [server.Message] object
     */
    public static Message fromServer(String message) {
        return new Message(message, "SERVER");
    }

    public String getSender() {
        return sender;
    }

    private String encodeString(String text) {
        if (text == null) return null;
        byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
        return Base64.getEncoder().encodeToString(bytes);
    }

    /**
     * Override toString() to return a consistent string representation notation that will be sent to the client
     *
     * @return encoded Message object as a string
     */
    @Override
    public String toString() {
        return
                "dateTime=" + encodeString(dateTime.toString()) +
                        ",chatType=" + chatType +
                        ",message=" + encodeString(message) +
                        ",sender=" + encodeString(sender);
    }
}