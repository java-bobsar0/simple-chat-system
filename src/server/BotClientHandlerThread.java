package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Handles interaction between the server and the bot client
 */
public class BotClientHandlerThread extends ClientHandlerFactoryThread {

    public BotClientHandlerThread(PrintWriter out, BufferedReader in) {
        super(out, in);
        this.setName("bot");
        new MessageBroadcasterThread(Message.fromServer("*** The chat BOT is now available for chatting! Start your message with '@bot ' to chat with the bot ***")).start();
    }

    @Override
    public void run() {
        ClientDatabase.INSTANCE.addClient(this);
        try {
            String inputLine;
            // communicate with the client by reading from and writing to the clientSocket
            while ((inputLine = in.readLine()) != null) {
                // remove the @ symbol from inputLine
                chatPrivately(inputLine.substring(1));
            }
        } catch (IOException e) {
            System.err.println("I/O connection error: " + e.getMessage());
            System.exit(1);
        }
    }
}