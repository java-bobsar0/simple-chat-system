package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.stream.Collectors;

/**
 * SERVER-SIDE CLASS
 * Handles client interaction with server
 */
public abstract class ClientHandlerFactoryThread extends Thread {

    protected PrintWriter out;
    protected BufferedReader in;

    /**
     * Instantiates the appropriate ClientHandler implementation
     *
     * @param out PrintWriter object for writing outputs to the client's socket
     * @param in  BufferedReader object for reading inputs from the client's socket
     *            <p>
     *            This method is only available to subclasses to ensure it is not used by other classes to create a ClientHandler
     */
    protected ClientHandlerFactoryThread(PrintWriter out, BufferedReader in) {
        this.out = out;
        this.in = in;
    }

    /**
     * Return the desired ClientHandler implementation class based on clientType criteria [Factory pattern]
     */
    public static ClientHandlerFactoryThread getClientHandler(Socket socket) {
        try {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String clientType = in.readLine();
            if (clientType.equalsIgnoreCase("bot")) {
                return new BotClientHandlerThread(out, in);
            } else {
                return new HumanClientHandlerThread(out, in);
            }
        } catch (IOException e) {
            System.out.println("I/O error in reading input: " + e.getMessage());
            System.exit(1);
        }
        return null;
    }

    /**
     * Implements private chat functionality when message beginning with @ character is sent
     *
     * @param content message including recipient name after the '@' character
     */
    protected void chatPrivately(String content) {
        // extract recipient from message content
        String[] contentArr = content.trim().split(" ");
        String recipient = contentArr[0];

        // extract plain message from content
        StringBuilder messageBuilder = new StringBuilder();
        for (int i = 1; i < contentArr.length; i++) {
            messageBuilder.append(contentArr[i]).append(" ");
        }
        String message = messageBuilder.toString().trim();

        if (recipient.equalsIgnoreCase("bot")) {
            if (!doesUsernameExist("bot")) {
                Message serverMsg = Message.fromServer("Sorry the chat BOT is not available right now. You will be notified once it's available!");
                outputMessage(serverMsg.toString());
                return;
            }
        }
        if (recipient.equalsIgnoreCase(this.getName())) {
            Message serverMsg = Message.fromServer("Sorry cannot send a message to yourself...");
            outputMessage(serverMsg.toString());
            return;
        }

        // lock INSTANCE to prevent access by other threads while iterating through list of clients
        synchronized (ClientDatabase.INSTANCE) {
            for (ClientHandlerFactoryThread client : ClientDatabase.INSTANCE.getClients()) {
                if (client.getName().equalsIgnoreCase(recipient)) {
                    // send private message to client as determined by recipient name
                    client.outputMessage(new PrivateMessage(message, this.getName()).toString());
                    return;
                }
            }
            Message serverMsg = Message.fromServer("User " + recipient + " does not exist...");
            outputMessage(serverMsg.toString());
        }
    }

    protected boolean doesUsernameExist(String username) {
        // lock database INSTANCE to prevent access by another thread while iterating through clients list
        synchronized (ClientDatabase.INSTANCE) {
            List<ClientHandlerFactoryThread> clients = ClientDatabase.INSTANCE.getClients();
            // get a list of all clients' username in lowercase
            List<String> usernameList = clients.stream().map(client -> client.getName().toLowerCase()).collect(Collectors.toList());

            return usernameList.contains(username.toLowerCase());
        }
    }

    public void outputMessage(String message) {
        out.println(message);
    }

    /**
     * Shut down client program
     *
     * @throws IOException when there's an I/O error in closing resources
     */
    protected void shutDownClient() throws IOException {
        synchronized (ClientDatabase.INSTANCE) {
            ClientDatabase.INSTANCE.removeClient(this);
            Message serverMsg = Message.fromServer(this.getName() + " has left the chat.");
            new MessageBroadcasterThread(serverMsg).start();
        }
        closeResources();
    }

    protected void closeResources() throws IOException {
        out.close();
        in.close();
    }

    /**
     * Abstract 'run' method to be implemented by all subclasses
     * This method is automatically called when the thread is started
     */
    @Override
    public abstract void run();
}