package server;

public class GroupMessage extends Message {

    public GroupMessage(String message, String sender) {
        super(message, ChatType.GROUP, sender);
    }
}