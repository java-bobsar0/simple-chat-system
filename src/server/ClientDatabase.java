package server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Holds a list of clients(threads) connected to the server and thread-safe methods used in modifying the list
 */
public enum ClientDatabase {
    // use single element enum type to implement thread-safe singleton as any enum value is instantiated only once in a Java program.
    INSTANCE;

    private final List<ClientHandlerFactoryThread> clients = new ArrayList<>();

    public synchronized List<ClientHandlerFactoryThread> getClients() {
        // return a list that cannot be modified externally. Can only use methods in this class to modify
        return Collections.unmodifiableList(clients);
    }

    public synchronized void addClient(ClientHandlerFactoryThread client) {
        clients.add(client);
        System.out.println("\n" + client.getName() + " has joined the room.");
        System.out.println("Total no of people in chat: " + clients.size());
    }

    public synchronized void removeClient(ClientHandlerFactoryThread client) {
        clients.remove(client);
        System.out.println("\n" + client.getName() + " has left the room.");
        System.out.println("Total no of people in chat: " + clients.size());
    }
}