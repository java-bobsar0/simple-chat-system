package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Handles interaction between the server and a human client
 */
public class HumanClientHandlerThread extends ClientHandlerFactoryThread {

    private final static String EXIT_COMMAND = "bye";

    public HumanClientHandlerThread(PrintWriter out, BufferedReader in) {
        super(out, in);
    }

    @Override
    public void run() {
        // get the socket's input and output streams and opens readers and writers on them
        // sending a message to the client involves writing information to the PrintWriter
        try {
            welcomeUser();
            // add client to clients database
            ClientDatabase.INSTANCE.addClient(this);

            String inputLine;

            // communicate with the client by reading from and writing to the clientSocket
            while ((inputLine = in.readLine()) != null) {
                if (inputLine.startsWith("@")) {
                    if (inputLine.length() == 1) {
                        outputMessage(Message.fromServer("Did you want a private conversation? Please include the name of the person after the @ symbol").toString());
                    } else {
                        chatPrivately(inputLine.substring(1));
                    }
                    continue;
                }
                // send a group message using the messageBroadcaster thread
                GroupMessage groupMessage = new GroupMessage(inputLine, this.getName());
                new MessageBroadcasterThread(groupMessage).start();

                if (inputLine.equalsIgnoreCase(EXIT_COMMAND)) {
                    shutDownClient();
                    break;
                }
            }
        } catch (IOException e) {
            System.err.println("I/O connection error: " + e.getMessage());
            System.exit(1);
        }
    }

    private void welcomeUser() throws IOException {
        String username;
        outputMessage(Message.fromServer("Welcome to the CW1 Chat system! Please enter your name below:").toString());

        do {
            username = in.readLine();
            if (username.equalsIgnoreCase("bot")) {
                outputMessage(Message.fromServer("Sorry, " + username + " is a reserved keyword for the chat bot. Please choose another name:").toString());
                continue;
            }
            // return to start of loop if name is blank or contains special characters
            if (username.isBlank() || !username.matches("[a-zA-Z0-9]*")) {
                outputMessage(Message.fromServer("Name must contain only alphabets and numbers. Please try again:").toString());
                continue;
            }
            if (!doesUsernameExist(username)) {
                this.setName(username);
                break;
            }
            outputMessage(Message.fromServer("A user named " + username + " already exists. Please choose another name").toString());
        } while (true);
        new MessageBroadcasterThread(Message.fromServer(this.getName() + " has joined the chat.")).start();

        String message = "Hello " + this.getName() + ", thanks for joining the chat. Enter 'bye' to exit the chat at any time.";
        if (doesUsernameExist("bot")) {
            message += " Begin a chat with '@bot' to initiate a conversation with the chat BOT.";
        }
        outputMessage(Message.fromServer(message + " Happy chatting!").toString());
    }
}