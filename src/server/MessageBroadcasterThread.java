package server;

import java.util.List;

/**
 * Thread responsible for broadcasting messages to all connected clients
 */
public class MessageBroadcasterThread extends Thread {

    private final Message message;

    MessageBroadcasterThread(Message message) {
        this.message = message;
    }

    @Override
    public void run() {
        synchronized (ClientDatabase.INSTANCE) {
            List<ClientHandlerFactoryThread> clients = ClientDatabase.INSTANCE.getClients();

            for (ClientHandlerFactoryThread client : clients) {
                // broadcast to everyone apart from sender and bot
                String name = client.getName();
                if (!name.equals(message.getSender()) && !name.equals("bot")) {
                    client.outputMessage(message.toString());
                }
            }
        }
    }
}