package server;

public enum ChatType {
    GROUP,
    PRIVATE;
}