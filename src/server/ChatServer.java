package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Duration;

/**
 * server.ChatServer responsible for establishing connections with multiple clients, and reading from and writing to their sockets and processing chat messages via the ClientHandler
 */
public class ChatServer {

    public static void main(String[] args) {
        int portNumber = getPortNumber(args);

        // create the ServerSocket object and BufferedReader in a try-with-resources statement to ensure the resources are closed after the program is finished with them
        try (ServerSocket serverSocket = new ServerSocket(portNumber);
             BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        ) {
            System.out.println("Server is up and listening on port " + portNumber + "...");

            while (serverSocket.isBound()) {
                // Spin new thread to shutdown server on "EXIT" command after 10seconds
                shutDownServerThread(stdIn, Duration.ofSeconds(10)).start();

                // listen to the socket to make a client connection and create a new socket for each client (bound to the same local port and has server remote address and remote port set to that of the client)
                // this allows the server to continue to listen for client connection requests on the original ServerSocket
                Socket clientSocket = serverSocket.accept();

                ClientHandlerFactoryThread client = ClientHandlerFactoryThread.getClientHandler(clientSocket);
                client.start();
            }
        } catch (IOException e) {
            System.err.println("I/O exception: " + e.getMessage());
            System.exit(1);
        }

    }

    /**
     * Shuts down server and terminates all connected clients
     *
     * @param stdIn    BufferedReader object used to read input messages from the commandline
     * @param duration duration after which clients will be disconnected
     * @return [Thread] that processes server exit
     */
    private static Thread shutDownServerThread(BufferedReader stdIn, Duration duration) {
        return new Thread(() -> {
            String inputLine;
            try {
                while ((inputLine = stdIn.readLine()) != null) {
                    if (inputLine.equals("EXIT")) {
                        System.out.println("Shutting down server...");
                        //broadcast a message that server is exiting and send null to terminate all connected client resources
                        new MessageBroadcasterThread(Message.fromServer("URGENT NOTICE: The server is shutting down for maintenance and program will terminate in 10 seconds. Please conclude all conversations and try to reconnect later in about 30min! Thank you.")).start();
                        Thread.sleep(duration.toMillis());
                        new MessageBroadcasterThread(null).start();

                        // use exit code 0 to indicate a successful graceful exit
                        System.exit(0);
                    }
                }
            } catch (IOException | InterruptedException e) {
                System.err.println("Error in shutting down server: " + e.getMessage());
                System.exit(1);
            }
        });
    }

    private static int getPortNumber(String[] valuesArr) {
        if (valuesArr.length == 2 && valuesArr[0].equals("-csp")) {
            try {
                int portNumber = Integer.parseInt(valuesArr[1]);
                if (portNumber < 1024) {
                    System.err.println("Please choose a port number between 1024 and 65,535");
                    System.exit(1);
                }
                return portNumber;
            } catch (NumberFormatException e) {
                System.err.println("Error in parsing port number. Ensure that the port number is a valid integer");
                System.exit(1);
            }
        } else if (valuesArr.length > 0) {
            System.err.println("Invalid optional parameter. Usage: java server.ChatServer -csp <port number>");
            System.exit(1);
        }
        return 14001;
    }
}