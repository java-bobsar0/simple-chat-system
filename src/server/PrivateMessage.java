package server;

public class PrivateMessage extends Message {
    public PrivateMessage(String message, String sender) {
        super(message, ChatType.PRIVATE, sender);
    }
}